export default class DocumentPacket {
  name: string | undefined;
  clock: string | undefined;
  constructor() {
    this.name = undefined;
    this.clock = undefined;
  }
}
