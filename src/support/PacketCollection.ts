import DocumentPacket from "./DocumentPacket";

export default class PacketCollection {
  collection: any[];
  constructor() {
    this.collection = [];
  }
  getOrCreatePacket(id: string) {
    let packet = this.collection.find((row) => row.id === id);
    if (!packet) {
      if (id === "document") {
        packet = new DocumentPacket();
      } else {
        packet = {
          id,
        };
      }

      this.collection.push(packet);
    }
    return packet;
  }
}
